package ru.t1.chernysheva.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
