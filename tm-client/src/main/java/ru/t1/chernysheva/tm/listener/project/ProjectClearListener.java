package ru.t1.chernysheva.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.dto.request.ProjectClearRequest;
import ru.t1.chernysheva.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Clear project list.";

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR PROJECTS]");

        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());

        projectEndpoint.clearProject(request);
    }

}
