package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.IProjectService;
import ru.t1.chernysheva.tm.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService
public class ProjectCollectionEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @WebMethod(operationName = "findAll")
    @GetMapping()
    public List<ProjectDTO> get() {
        return projectService.findAll();
    }

    @WebMethod(operationName = "saveCollection")
    @PostMapping
    public void post (
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(projects);
    }

    @WebMethod(operationName = "updateCollection")
    @PutMapping
    public void put(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(projects);
    }

    @WebMethod(operationName = "deleteCollection")
    @DeleteMapping()
    public void delete(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.removeAll(projects);
    }


}
