package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.ITaskService;
import ru.t1.chernysheva.tm.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService
public class TaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @WebMethod(operationName = "findOne")
    @GetMapping("/{id}")
    public TaskDTO get(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(id);
    }

    @WebMethod(operationName = "saveOne")
    @PostMapping
    public void post(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(task);
    }

    @WebMethod(operationName = "updateOne")
    @PutMapping
    public void put(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(task);
    }

    @WebMethod(operationName = "deleteOne")
    @DeleteMapping("/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(id);
    }

}
