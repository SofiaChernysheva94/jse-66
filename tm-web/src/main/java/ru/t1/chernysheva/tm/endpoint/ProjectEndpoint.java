package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.IProjectService;
import ru.t1.chernysheva.tm.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/project")
@WebService
public class ProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @WebMethod(operationName = "findOne")
    @GetMapping("/{id}")
    public ProjectDTO get(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        return projectService.findOneById(id);
    }

    @WebMethod(operationName = "saveOne")
    @PostMapping
    public void post(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody ProjectDTO project) {
        projectService.save(project);
    }

    @WebMethod(operationName = "updateOne")
    @PutMapping
    public void put(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody ProjectDTO project) {
        projectService.save(project);
    }

    @WebMethod(operationName = "deleteOne")
    @DeleteMapping("/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(id);
    }


}
