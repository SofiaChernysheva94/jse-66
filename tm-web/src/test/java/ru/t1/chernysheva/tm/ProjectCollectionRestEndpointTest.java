package ru.t1.chernysheva.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.client.ProjectCollectionRestEndpointClient;
import ru.t1.chernysheva.tm.marker.IntegrationCategory;
import ru.t1.chernysheva.tm.model.ProjectDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectCollectionRestEndpointTest {

    @NotNull
    final ProjectCollectionRestEndpointClient client = ProjectCollectionRestEndpointClient.client();

    @NotNull
    final ProjectDTO projectAutoFirst = new ProjectDTO("Project Test 1");

    @NotNull
    final ProjectDTO projectAutoSecond = new ProjectDTO("Project Test 2");

    @NotNull
    final ProjectDTO projectAutoThird = new ProjectDTO("Project Test 3");

    @NotNull
    final List<ProjectDTO> projectCollection = Arrays.asList(projectAutoFirst, projectAutoSecond, projectAutoThird);

    @NotNull
    final ProjectDTO projectManualFirst = new ProjectDTO("Project Manual Test 1");

    @NotNull
    final ProjectDTO projectManualSecond = new ProjectDTO("Project Manual Test 2");

    @NotNull
    final List<ProjectDTO> projectCollectionSecond = Arrays.asList(projectManualFirst, projectManualSecond);

    @Before
    public void init() {
        client.post(projectCollection);
    }

    @After
    public void clear() {
        client.delete(client.get());
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get());
        for (final ProjectDTO project : projectCollection) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPost() {
        Assert.assertEquals(3, client.get().size());
        client.post(projectCollectionSecond);
        Assert.assertNotNull(client.get());
        Assert.assertEquals(5, client.get().size());
        for (final ProjectDTO project : projectCollectionSecond) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPut() {
        for (final ProjectDTO project : client.get()) {
            Assert.assertNull(project.getDescription());
        }
        projectCollection.get(0).setDescription("New Description 1");
        projectCollection.get(1).setDescription("New Description 2");
        projectCollection.get(2).setDescription("New Description 3");
        client.put(projectCollection);
        for (final ProjectDTO project : client.get()) {
            Assert.assertNotNull(project.getDescription());
        }
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get());
        client.delete(projectCollection);
        Assert.assertEquals(Collections.emptyList(), client.get());
    }

}
