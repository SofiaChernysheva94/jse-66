package ru.t1.chernysheva.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.model.Project;

@Repository
public interface IProjectEntityRepository extends IModelUserOwnedRepository<Project> {

}
