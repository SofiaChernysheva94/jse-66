package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void invalidate(SessionDTO session) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token);

}
