package ru.t1.chernysheva.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.EntitySort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.model.Task;

import java.util.Comparator;

public interface TaskConstant {

    int INIT_COUNT_TASKS = 5;

    @NotNull
    String USER_ID_1 = "tst-usr-task-id-1";

    @NotNull
    String USER_ID_2 = "tst-usr-task-id-2";

    @Nullable
    TaskDTO NULLABLE_TASK = null;

    @Nullable
    Task NULLABLE_TASK_MODEL = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_TASK_ID = null;

    @NotNull
    String EMPTY_TASK_ID = "";

    @NotNull
    EntitySort CREATED_ENTITY_SORT = EntitySort.BY_CREATED;

    @NotNull
    EntitySort NAME_ENTITY_SORT = EntitySort.BY_NAME;

    @NotNull
    EntitySort STATUS_ENTITY_SORT = EntitySort.BY_STATUS;

    @Nullable
    EntitySort NULLABLE_ENTITY_SORT = null;

    @NotNull
    Comparator<TaskDTO> TASK_COMPARATOR = CREATED_ENTITY_SORT.getComparator();

    @Nullable
    Comparator<TaskDTO> NULLABLE_COMPARATOR = null;

    @Nullable
    Integer NULLABLE_INDEX = null;

    @NotNull
    Status IN_PROGRESS_STATUS = Status.IN_PROGRESS;

}
